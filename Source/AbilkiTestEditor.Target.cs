using UnrealBuildTool;
using System.Collections.Generic;

public class AbilkiTestEditorTarget : TargetRules
{
	public AbilkiTestEditorTarget(TargetInfo Target) : base(Target)
	{
        DefaultBuildSettings = BuildSettingsVersion.V2;

        Type = TargetType.Editor;
		ExtraModuleNames.Add("AbilkiTest");
	}
}
