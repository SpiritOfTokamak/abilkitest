using UnrealBuildTool;
using System.Collections.Generic;

public class AbilkiTestTarget : TargetRules
{
	public AbilkiTestTarget(TargetInfo Target) : base(Target)
	{
        DefaultBuildSettings = BuildSettingsVersion.V2;

        Type = TargetType.Game;
		ExtraModuleNames.Add("AbilkiTest");
	}
}
