using UnrealBuildTool;

public class AbilkiTest : ModuleRules
{
	public AbilkiTest(ReadOnlyTargetRules Target) : base(Target)
	{
        DefaultBuildSettings = BuildSettingsVersion.V2;

        PublicDependencyModuleNames.AddRange(
            new string[] 
            {
                "Core",
                "CoreUObject",
                "Engine",
                "InputCore",
                "HeadMountedDisplay",
                "GameplayAbilities",
                "GameplayTags",
                "GameplayTasks",
                "AIModule"
            });
	}
}
