#include "AbilkiTestGameMode.h"
#include "AbilkiTestHUD.h"
#include "AbilkiTestCharacter.h"
#include "UObject/ConstructorHelpers.h"

AAbilkiTestGameMode::AAbilkiTestGameMode()
{
	HUDClass = AAbilkiTestHUD::StaticClass();
}
