#pragma once

#include "CoreMinimal.h"
#include "Abilities/GameplayAbilityTargetActor.h"
#include "AroundTargetActor.generated.h"

UCLASS()
class ABILKITEST_API AAroundTargetActor : public AGameplayAbilityTargetActor
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (ExposeOnSpawn = true))
	float Radius = 200.0f;

	void StartTargeting(UGameplayAbility* Ability) override;
	void ConfirmTargetingAndContinue() override;

protected:
	bool GetOverlappedActorsFromPlayerViewPoint(TArray<TWeakObjectPtr<AActor>>& OverlappedActors);
	void MakeCollisionQueryParams(FCollisionQueryParams& QueryParams, bool bTraceComplex);
};
