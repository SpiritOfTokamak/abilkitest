#pragma once

#include "CoreMinimal.h"
#include "Abilities/GameplayAbilityTargetActor.h"
#include "GroundBlastTargetActor.generated.h"

class UDecalComponent;
class USceneComponent;

UCLASS()
class ABILKITEST_API AGroundBlastTargetActor : public AGameplayAbilityTargetActor
{
	GENERATED_BODY()

public:
	AGroundBlastTargetActor();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float TraceLength = 10000.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (ExposeOnSpawn = true))
	float Radius = 200.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UDecalComponent* Decal = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	USceneComponent* RootComp = nullptr;

	void StartTargeting(UGameplayAbility* Ability) override;
	void ConfirmTargetingAndContinue() override;
	void Tick(float DeltaSeconds) override;

	UFUNCTION(BlueprintCallable)
	bool GetPlayerLookingPoint(FVector& OutViewPoint);

protected:
	void MakeCollisionQueryParams(FCollisionQueryParams& QueryParams, bool bTraceComplex = true);
	bool GetOverlappedActorsFromPlayerViewPoint(TArray<TWeakObjectPtr<AActor>>& OverlappedActors);
};
