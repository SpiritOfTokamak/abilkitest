#pragma once

#include "CoreMinimal.h"
#include "AbilityTypes.generated.h"

class UGameplayAbilityBase;

UENUM(BlueprintType)
enum class EAbilityCostType : uint8
{
	Health,
	Mana,
	Strength
};

USTRUCT(BlueprintType)
struct FGameplayAbilityInfo
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float CooldownDuration = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Cost = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	EAbilityCostType CostType = EAbilityCostType::Mana;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UMaterialInstance* UIMaterial = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<UGameplayAbilityBase> AbilityClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool PromoteToUI = true;
};
