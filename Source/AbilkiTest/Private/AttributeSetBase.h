#pragma once

#include "CoreMinimal.h"
#include "AttributeSet.h"
#include "AttributeSetBase.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FParameterChangedSignature, float, Health, float, MaxHealth);

UCLASS()
class UAttributeSetBase : public UAttributeSet
{
	GENERATED_BODY()

public:
	UAttributeSetBase();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FGameplayAttributeData Health { 200.0f };

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FGameplayAttributeData MaxHealth { 200.0f };

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FGameplayAttributeData Mana { 100.0f };

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FGameplayAttributeData MaxMana { 150.0f };

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FGameplayAttributeData Strength { 250.0f };

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FGameplayAttributeData MaxStrength { 250.0f };

	UPROPERTY(EditAnywhere, BlueprintAssignable)
	FParameterChangedSignature HealthChanged;

	UPROPERTY(EditAnywhere, BlueprintAssignable)
	FParameterChangedSignature ManaChanged;

	UPROPERTY(EditAnywhere, BlueprintAssignable)
	FParameterChangedSignature StrengthChanged;

	void PostGameplayEffectExecute(const struct FGameplayEffectModCallbackData& Data) override;
	void ProcessAttributeChange(FGameplayAttributeData& Current, FGameplayAttributeData& Max, FParameterChangedSignature& Delegate);
};
