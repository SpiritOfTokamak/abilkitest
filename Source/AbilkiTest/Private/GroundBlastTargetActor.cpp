#include "GroundBlastTargetActor.h"

#include "Abilities/GameplayAbility.h"
#include "Components/DecalComponent.h"
#include "DrawDebugHelpers.h"

AGroundBlastTargetActor::AGroundBlastTargetActor()
{
	RootComp = CreateDefaultSubobject<USceneComponent>("RootComponent");
	SetRootComponent(RootComp);

	Decal = CreateDefaultSubobject<UDecalComponent>("Decal");
	Decal->DecalSize = FVector(Radius);
	Decal->SetupAttachment(RootComp);

	PrimaryActorTick.bCanEverTick = true;
}

void AGroundBlastTargetActor::StartTargeting(UGameplayAbility* Ability)
{
	OwningAbility = Ability;
	MasterPC = Ability->GetOwningActorFromActorInfo()->GetInstigatorController<APlayerController>();
	Decal->DecalSize = FVector(Radius);
}

void AGroundBlastTargetActor::ConfirmTargetingAndContinue()
{
	TArray<TWeakObjectPtr<AActor>> OverlappedActors;
	GetOverlappedActorsFromPlayerViewPoint(OverlappedActors);

	FGameplayAbilityTargetDataHandle TargetData = StartLocation.MakeTargetDataHandleFromActors(OverlappedActors);

	FGameplayAbilityTargetData_LocationInfo* Location = new FGameplayAbilityTargetData_LocationInfo();
	Location->TargetLocation.LiteralTransform = Decal->GetComponentTransform();
	Location->TargetLocation.LocationType = EGameplayAbilityTargetingLocationType::LiteralTransform;
	TargetData.Add(Location);

	TargetDataReadyDelegate.Broadcast(TargetData);
}

void AGroundBlastTargetActor::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	FVector PlayerViewPoint;
	if (GetPlayerLookingPoint(PlayerViewPoint))
	{
		//DrawDebugSphere(GetWorld(), PlayerViewPoint, Radius, 32, FColor::Red, false, -1.0f, 0, 5.0f);

		Decal->SetWorldLocation(PlayerViewPoint);
	}
}

bool AGroundBlastTargetActor::GetPlayerLookingPoint(FVector& OutViewPoint)
{
	FVector ViewPoint;
	FRotator ViewRotation;
	MasterPC->GetPlayerViewPoint(ViewPoint, ViewRotation);

	FCollisionQueryParams QueryParams;
	MakeCollisionQueryParams(QueryParams);

	FHitResult HitResult;
	bool TraceResult = GetWorld()->LineTraceSingleByChannel(
		HitResult,
		ViewPoint,
		ViewPoint * ViewRotation.Vector() * TraceLength,
		ECC_Visibility,
		QueryParams);

	if (TraceResult)
	{
		OutViewPoint = HitResult.ImpactPoint;
	}

	return TraceResult;
}

void AGroundBlastTargetActor::MakeCollisionQueryParams(FCollisionQueryParams& QueryParams, bool bTraceComplex)
{
	QueryParams.bTraceComplex = bTraceComplex;

	APawn* MasterPawn = MasterPC->GetPawn();
	if (MasterPC)
	{
		QueryParams.AddIgnoredActor(MasterPawn);
	}
}

bool AGroundBlastTargetActor::GetOverlappedActorsFromPlayerViewPoint(TArray<TWeakObjectPtr<AActor>>& OverlappedActors)
{
	FVector PlayerViewPoint;
	if (GetPlayerLookingPoint(PlayerViewPoint))
	{
		TArray<FOverlapResult> Overlaps;

		FCollisionQueryParams QueryParams;
		MakeCollisionQueryParams(QueryParams, false);
		QueryParams.bReturnPhysicalMaterial = false;

		bool TraceResult = GetWorld()->OverlapMultiByObjectType(
			Overlaps,
			PlayerViewPoint,
			FQuat::Identity,
			FCollisionObjectQueryParams(ECC_Pawn),
			FCollisionShape::MakeSphere(Radius),
			QueryParams);

		if (TraceResult)
		{
			for (const FOverlapResult& OverlapResult : Overlaps)
			{
				APawn* OverlappedPawn = Cast<APawn>(OverlapResult.GetActor());
				if (OverlappedPawn != nullptr)
				{
					OverlappedActors.AddUnique(OverlappedPawn);
				}
			}
		}

		return TraceResult;
	}

	return false;
}
