#include "GameplayAbilityBase.h"

FGameplayAbilityInfo UGameplayAbilityBase::GetAbilityInfo()
{
	UGameplayEffect* Cooldown = GetCooldownGameplayEffect();
	UGameplayEffect* Cost = GetCostGameplayEffect();

	FGameplayAbilityInfo Info;
	Info.UIMaterial = UIMaterial;
	Info.AbilityClass = GetClass();
	Info.PromoteToUI = PromoteToUI;

	if (Cooldown != nullptr)
	{
		Cooldown->DurationMagnitude.GetStaticMagnitudeIfPossible(1, Info.CooldownDuration);
	}

	if (Cost != nullptr && Cost->Modifiers.Num() > 0)
	{
		const FGameplayModifierInfo& EffectInfo = Cost->Modifiers[0];
		EffectInfo.ModifierMagnitude.GetStaticMagnitudeIfPossible(1, Info.Cost);

		if (EffectInfo.Attribute.AttributeName == "Health")
		{
			Info.CostType = EAbilityCostType::Health;
		}
		else if (EffectInfo.Attribute.AttributeName == "Mana")
		{
			Info.CostType = EAbilityCostType::Mana;
		}
		else if (EffectInfo.Attribute.AttributeName == "Strength")
		{
			Info.CostType = EAbilityCostType::Strength;
		}
	}

	return Info;
}
