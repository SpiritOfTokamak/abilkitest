#include "AttributeSetBase.h"

#include "GameplayEffectExtension.h"
#include "GameplayEffectTypes.h"

UAttributeSetBase::UAttributeSetBase() = default;

void UAttributeSetBase::PostGameplayEffectExecute(const FGameplayEffectModCallbackData& Data)
{
	FProperty* Property = Data.EvaluatedData.Attribute.GetUProperty();

	if (Property == FindFieldChecked<FProperty>(StaticClass(), GET_MEMBER_NAME_CHECKED(UAttributeSetBase, Health)))
	{
		ProcessAttributeChange(Health, MaxHealth, HealthChanged);
	}
	else if (Property == FindFieldChecked<FProperty>(StaticClass(), GET_MEMBER_NAME_CHECKED(UAttributeSetBase, Mana)))
	{
		ProcessAttributeChange(Mana, MaxMana, ManaChanged);
	}
	else if (Property == FindFieldChecked<FProperty>(StaticClass(), GET_MEMBER_NAME_CHECKED(UAttributeSetBase, Strength)))
	{
		ProcessAttributeChange(Strength, MaxStrength, StrengthChanged);
	}
}

void UAttributeSetBase::ProcessAttributeChange(FGameplayAttributeData& Current, FGameplayAttributeData& Max, FParameterChangedSignature& Delegate)
{
	Current.SetCurrentValue(FMath::Clamp(Current.GetCurrentValue(), 0.0f, Max.GetCurrentValue()));
	Current.SetBaseValue(FMath::Clamp(Current.GetBaseValue(), 0.0f, Max.GetBaseValue()));

	Delegate.Broadcast(Current.GetCurrentValue(), Max.GetCurrentValue());
}
