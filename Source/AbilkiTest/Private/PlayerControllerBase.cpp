#include "PlayerControllerBase.h"

#include "AbilkiTest/AbilkiTestGameMode.h"
#include "CharacterBase.h"

void APlayerControllerBase::OnPawnDead()
{
	ACharacterBase* OwnCharacter = Cast<ACharacterBase>(GetCharacter());
	if (OwnCharacter && OwnCharacter->bShouldResurrect)
	{
		AGameModeBase* GameMode = GetWorld()->GetAuthGameMode();
		if (GameMode)
		{
			UnPossess();
			GameMode->RestartPlayer(this);
		}
	}
}
