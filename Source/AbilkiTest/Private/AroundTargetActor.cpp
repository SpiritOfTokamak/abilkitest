#include "AroundTargetActor.h"
#include "Abilities/GameplayAbility.h"

void AAroundTargetActor::StartTargeting(UGameplayAbility* Ability)
{
	OwningAbility = Ability;
	MasterPC = Ability->GetOwningActorFromActorInfo()->GetInstigatorController<APlayerController>();
}

void AAroundTargetActor::ConfirmTargetingAndContinue()
{
	TArray<TWeakObjectPtr<AActor>> OverlappedActors;
	if (!GetOverlappedActorsFromPlayerViewPoint(OverlappedActors))
	{
		TargetDataReadyDelegate.Broadcast(FGameplayAbilityTargetDataHandle());
		return;
	}

	FGameplayAbilityTargetDataHandle TargetData = StartLocation.MakeTargetDataHandleFromActors(OverlappedActors);
	TargetDataReadyDelegate.Broadcast(TargetData);
}

bool AAroundTargetActor::GetOverlappedActorsFromPlayerViewPoint(TArray<TWeakObjectPtr<AActor>>& OverlappedActors)
{
	APawn* MasterPawn = MasterPC->GetPawn();
	if (!MasterPawn)
	{
		return false;
	}

	FVector PlayerViewPoint = MasterPawn->GetActorLocation();
	TArray<FOverlapResult> Overlaps;

	FCollisionQueryParams QueryParams;
	MakeCollisionQueryParams(QueryParams, false);
	QueryParams.bReturnPhysicalMaterial = false;

	bool TraceResult = GetWorld()->OverlapMultiByObjectType(
		Overlaps,
		PlayerViewPoint,
		FQuat::Identity,
		FCollisionObjectQueryParams(ECC_Pawn),
		FCollisionShape::MakeSphere(Radius),
		QueryParams);

	if (TraceResult)
	{
		for (const FOverlapResult& OverlapResult : Overlaps)
		{
			APawn* OverlappedPawn = Cast<APawn>(OverlapResult.GetActor());
			if (OverlappedPawn != nullptr)
			{
				OverlappedActors.AddUnique(OverlappedPawn);
			}
		}
	}

	return TraceResult;
}

void AAroundTargetActor::MakeCollisionQueryParams(FCollisionQueryParams& QueryParams, bool bTraceComplex)
{
	QueryParams.bTraceComplex = bTraceComplex;

	APawn* MasterPawn = MasterPC->GetPawn();
	if (MasterPC)
	{
		QueryParams.AddIgnoredActor(MasterPawn);
	}
}