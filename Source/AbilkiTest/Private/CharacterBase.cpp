#include "CharacterBase.h"

#include "AbilitySystemComponent.h"
#include "AttributeSetBase.h"
#include "AIController.h"
#include "BrainComponent.h"
#include "GameFramework/InputSettings.h"

#include "GameplayAbilityBase.h"
#include "PlayerControllerBase.h"

ACharacterBase::ACharacterBase()
{
	AbilitySystemComp = CreateDefaultSubobject<UAbilitySystemComponent>(TEXT("AbilitySystemComp"));
	AttributeSetBaseComp = CreateDefaultSubobject<UAttributeSetBase>(TEXT("AttributeSetBaseComp"));

	AttributeSetBaseComp->HealthChanged.AddDynamic(this, &ACharacterBase::OnHealthChange);
	AttributeSetBaseComp->ManaChanged.AddDynamic(this, &ACharacterBase::OnManaChange);
	AttributeSetBaseComp->StrengthChanged.AddDynamic(this, &ACharacterBase::OnStrengthChange);
	PrimaryActorTick.bCanEverTick = true;
}

void ACharacterBase::BeginPlay()
{
	Super::BeginPlay();
	DetermineTeamId();

	AbilitySystemComp->InitAbilityActorInfo(this, this);
	FGameplayEffectContextHandle EffectContext = AbilitySystemComp->MakeEffectContext();

	for (TSubclassOf<UGameplayAbilityBase> Ability : GameplayAbilities)
	{
		AcquireAbility(Ability);
	}

	for (TSubclassOf<UGameplayEffect> Effect : StartUpEffects)
	{
		AbilitySystemComp->ApplyGameplayEffectToSelf(Effect.GetDefaultObject(), 1, EffectContext);
	}
}

void ACharacterBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ACharacterBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	if (FPlatformMisc::SupportsTouchInput() || GetDefault<UInputSettings>()->bUseMouseForTouch)
	{
		EnableTouchscreenMovement(PlayerInputComponent);
	}
}

void ACharacterBase::AcquireAbility(TSubclassOf<UGameplayAbilityBase> Ability)
{
	if (HasAuthority() && Ability != nullptr)
	{
		FGameplayAbilitySpec AbilitySpec(Ability, 1, 0);
		AbilitySystemComp->GiveAbility(AbilitySpec);
	}

	AddGameplayAbilityToUI(Ability);
}

void ACharacterBase::OnHealthChange(float Current, float Max)
{
	if (bDied)
	{
		return;
	}

	if (Current == 0.0f)
	{
		Dead();
	}

	if (Current == Max)
	{
		AddGameplayTag(FullHealthTag);
	}
	else if (AbilitySystemComp->HasMatchingGameplayTag(FullHealthTag))
	{
		RemoveGameplayTag(FullHealthTag);
	}

	BP_OnHealthChange(Current, Max);
}

void ACharacterBase::OnManaChange(float Current, float Max)
{
	BP_OnManaChange(Current, Max);
}

void ACharacterBase::OnStrengthChange(float Current, float Max)
{
	BP_OnStrengthChange(Current, Max);
}

void ACharacterBase::AddGameplayTag(FGameplayTag Tag)
{
	AbilitySystemComp->AddLooseGameplayTag(Tag);
	AbilitySystemComp->SetTagMapCount(Tag, 1);
}

void ACharacterBase::RemoveGameplayTag(FGameplayTag Tag)
{
	AbilitySystemComp->RemoveLooseGameplayTag(Tag);
}

void ACharacterBase::HitStun(float Duration)
{
	DisableInputControl();
	GetWorldTimerManager().SetTimer(StunHandle, this, &ACharacterBase::EnableInputControl, Duration, false);
}

void ACharacterBase::DetermineTeamId()
{
	if (GetController() && GetController()->IsPlayerController())
	{
		TeamId = 0;
	}
}

void ACharacterBase::Dead()
{
	bDied = true;
	DisableInputControl();
	BP_OnDie();

	APlayerControllerBase* PC = Cast<APlayerControllerBase>(GetController());
	if (PC)
	{
		PC->OnPawnDead();
	}
}

void ACharacterBase::DisableInputControl()
{
	APlayerControllerBase* PC = Cast<APlayerControllerBase>(GetController());
	if (PC)
	{
		PC->DisableInput(PC);
	}

	AAIController* AIC = Cast<AAIController>(GetController());
	if (AIC)
	{
		AIC->GetBrainComponent()->StopLogic(TEXT("Dead"));
	}
}

void ACharacterBase::EnableInputControl()
{
	if (!bDied)
	{
		APlayerControllerBase* PC = Cast<APlayerControllerBase>(GetController());
		if (PC)
		{
			PC->EnableInput(PC);
		}

		AAIController* AIC = Cast<AAIController>(GetController());
		if (AIC)
		{
			AIC->GetBrainComponent()->RestartLogic();
		}
	}
}

void ACharacterBase::AddGameplayAbilityToUI(TSubclassOf<UGameplayAbilityBase> Ability)
{
	APlayerControllerBase* PC = Cast<APlayerControllerBase>(GetController());

	if (PC && Ability)
	{
		FGameplayAbilityInfo AbilityInfo = Ability.GetDefaultObject()->GetAbilityInfo();
		if (AbilityInfo.PromoteToUI)
		{
			PC->AddAbilityToUI(AbilityInfo);
		}
	}
}

void ACharacterBase::EnableTouchscreenMovement(UInputComponent* PlayerInputComponent)
{
	PlayerInputComponent->BindTouch(EInputEvent::IE_Pressed, this, &ACharacterBase::BeginTouch);
	PlayerInputComponent->BindTouch(EInputEvent::IE_Released, this, &ACharacterBase::EndTouch);
	PlayerInputComponent->BindTouch(EInputEvent::IE_Repeat, this, &ACharacterBase::TouchUpdate);
}

void ACharacterBase::BeginTouch(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	if (FingerIndex >= SimultaneouslyTouches)
		return;

	TouchData& TouchItem = TouchItems[FingerIndex];
	if (TouchItem.bIsPressed)
		return;

	TouchItem.bIsPressed = true;
	TouchItem.Location = Location;
	TouchItem.bMoved = false;
}

void ACharacterBase::EndTouch(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	if (FingerIndex >= SimultaneouslyTouches)
		return;
	TouchItems[FingerIndex].bIsPressed = false;
}

void ACharacterBase::TouchUpdate(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	if (FingerIndex >= SimultaneouslyTouches)
		return;

	TouchData& TouchItem = TouchItems[FingerIndex];
	if (!TouchItem.bIsPressed)
		return;

	UGameViewportClient* ViewportClient = GetWorld()->GetGameViewport();
	if (ViewportClient != nullptr)
	{
		FVector MoveDelta = Location - TouchItem.Location;
		FVector2D ScreenSize;
		ViewportClient->GetViewportSize(ScreenSize);
		FVector2D ScaledDelta = FVector2D(MoveDelta.X, MoveDelta.Y) / ScreenSize;
		if (FMath::Abs(ScaledDelta.X) >= 1.0 / ScreenSize.X)
		{
			TouchItem.bMoved = true;
			float Value = ScaledDelta.X * BaseTurnRate;
			AddControllerYawInput(Value);
		}
		if (FMath::Abs(ScaledDelta.Y) >= 1.0 / ScreenSize.Y)
		{
			TouchItem.bMoved = true;
			float Value = ScaledDelta.Y * BaseLookUpRate;
			AddControllerPitchInput(Value);
		}
		TouchItem.Location = Location;
	}
	TouchItem.Location = Location;
}

