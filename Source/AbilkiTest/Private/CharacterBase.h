#pragma once

#include "CoreMinimal.h"
#include "AbilitySystemInterface.h"
#include "Abilities/GameplayAbility.h"
#include "GameFramework/Character.h"
#include "CharacterBase.generated.h"

class UAbilitySystemComponent;
class UAttributeSetBase;
class UGameplayAbilityBase;

struct TouchData
{
	FVector Location = FVector::ZeroVector;
	bool bIsPressed = false;
	bool bMoved = false;
};

UCLASS()
class ACharacterBase : public ACharacter, public IAbilitySystemInterface
{
	GENERATED_BODY()

public:
	ACharacterBase();

protected:
	void BeginPlay() override;

public:	
	void Tick(float DeltaTime) override;
	void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	UAbilitySystemComponent* GetAbilitySystemComponent() const override { return AbilitySystemComp; }

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Abilities")
	UAbilitySystemComponent* AbilitySystemComp;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Abilities")
	UAttributeSetBase* AttributeSetBaseComp;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Abilities")
	TArray<TSubclassOf<UGameplayAbilityBase>> GameplayAbilities;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Abilities")
	TArray<TSubclassOf<UGameplayEffect>> StartUpEffects;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Character")
	bool bDied = false;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Character")
	bool bShouldResurrect = false;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Character")
	uint8 TeamId = 255;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Character")
	FGameplayTag FullHealthTag;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Camera")
	float BaseTurnRate = 270.0f;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Camera")
	float BaseLookUpRate = 135.0f;

	UFUNCTION(BlueprintCallable)
	void AcquireAbility(TSubclassOf<UGameplayAbilityBase> Ability);

	UFUNCTION(BlueprintCallable)
	bool IsHostile(ACharacterBase* Other) { return TeamId != Other->TeamId; }

	UFUNCTION()
	void OnHealthChange(float Current, float Max);

	UFUNCTION()
	void OnManaChange(float Current, float Max);

	UFUNCTION()
	void OnStrengthChange(float Current, float Max);

	UFUNCTION(BlueprintImplementableEvent)
	void BP_OnManaChange(float Current, float Max);

	UFUNCTION(BlueprintImplementableEvent)
	void BP_OnStrengthChange(float Current, float Max);

	UFUNCTION(BlueprintImplementableEvent)
	void BP_OnHealthChange(float Current, float Max);

	UFUNCTION(BlueprintImplementableEvent)
	void BP_OnDie();

	UFUNCTION(BlueprintCallable)
	void AddGameplayTag(FGameplayTag Tag);

	UFUNCTION(BlueprintCallable)
	void RemoveGameplayTag(FGameplayTag Tag);

	UFUNCTION(BlueprintCallable)
	void HitStun(float Duration);

private:
	void DetermineTeamId();
	void Dead();

	void DisableInputControl();
	void EnableInputControl();
	void AddGameplayAbilityToUI(TSubclassOf<UGameplayAbilityBase> Ability);

	void EnableTouchscreenMovement(UInputComponent* PlayerInputComponent);
	void BeginTouch(const ETouchIndex::Type FingerIndex, const FVector Location);
	void EndTouch(const ETouchIndex::Type FingerIndex, const FVector Location);
	void TouchUpdate(const ETouchIndex::Type FingerIndex, const FVector Location);

	// support 4 simultaneously touches
	static const size_t SimultaneouslyTouches = 4;
	TouchData TouchItems[SimultaneouslyTouches];
	FTimerHandle StunHandle;
};
