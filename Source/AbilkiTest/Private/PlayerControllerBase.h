#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "AbilityTypes.h"
#include "PlayerControllerBase.generated.h"

UCLASS()
class APlayerControllerBase : public APlayerController
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintImplementableEvent)
	void AddAbilityToUI(FGameplayAbilityInfo AbilityInfo);

	void OnPawnDead();
};
