import argparse
import os
import AutomationCommon


def generate_gameversion_h(major_version, minor_version, build_num, branch, revision):
    content = []
    content.append('// This is auto-generated file. Dont commit it\n')
    content.append('#pragma once\n')
    content.append('const int GameMajorVersion = %s;\n' % major_version)
    content.append('const int GameMinorVersion = %s;\n' % minor_version)
    content.append('const int GameBuildNumber = %s;\n' % build_num)
    content.append('const char* GameRevision = \"%s@%s\";\n' % (branch, revision))

    version_file_path = os.path.join(AutomationCommon.get_project_root(), 'Source', AutomationCommon.get_project_name(), 'Public', 'GameVersion.h')
    with open(version_file_path, 'w') as version_file:
        version_file.writelines(content)


def parse_args():
    config = AutomationCommon.get_config()
    # get env (platform is not important)
    env = AutomationCommon.get_environment('win64')

    parser = argparse.ArgumentParser(description='Update game version')
    parser.add_argument('--major_version', default=config['MajorVersion'])
    parser.add_argument('--minor_version', default=config['MinorVersion'])
    parser.add_argument('--branch', default=env['branch'])
    parser.add_argument('--revision', default=env['commit'])
    parser.add_argument('--build_num', default=env['build_num'])
    parser.add_argument('--use_store_offset', action='store_true')

    return parser.parse_args()


def to_int(obj):
    return int(obj) if obj else 0


def change_app_version(major_version, minor_version, build_num, use_store_offset):
    final_build_num = to_int(build_num)
    if use_store_offset:
        final_build_num *= 10

    engine_config = AutomationCommon.load_config('DefaultEngine.ini')

    AutomationCommon.override_config_value(engine_config, 'VersionDisplayName', f'{to_int(major_version)}.{to_int(minor_version)}')
    AutomationCommon.override_config_value(engine_config, 'StoreVersion', f'{final_build_num}')

    # make store offset for arm64
    if use_store_offset:
        AutomationCommon.override_config_value(engine_config, 'StoreVersionOffsetArm64', '1')

    AutomationCommon.save_config('DefaultEngine.ini', engine_config)


if __name__ == '__main__':
    args = parse_args()

    generate_gameversion_h(args.major_version, args.minor_version, args.build_num, args.branch, args.revision)
    change_app_version(args.major_version, args.minor_version, args.build_num, args.use_store_offset)
