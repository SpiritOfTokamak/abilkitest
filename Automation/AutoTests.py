import argparse
import glob
import os
import shutil
import subprocess
import sys
import AutomationCommon
from os.path import expanduser


def make_log_filename(test_suite_name):
    test_suite_normalized_name = test_suite_name.replace('\\', '_').replace('/', '_')
    return f'Run_{test_suite_normalized_name}.log'


def get_game_path(env):
    appname = '%s%s' % (env['project_name'], env['exe_extension'])

    if env['host_platform'] == 'Win64':
        return '%s/%s' % (env['platform_release_path'], appname)
    elif env['host_platform'] == 'Mac':
        return '%s/%s.app/Contents/MacOS/%s' % (env['platform_release_path'], appname, appname)


def get_log_path(env):
    if env['host_platform'] == 'Win64':
        return '%s/%s/Saved/Logs' % (env['platform_release_path'], env['project_name'])
    elif env['host_platform'] == 'Mac':
        return '%s/Library/Logs/%s' % (expanduser('~'), env['project_name'])


def write_logs(test_suite_name, env):
    logsPath = get_log_path(env)
    logFile = os.path.join(logsPath, make_log_filename(test_suite_name))

    output_path = '%s/TestLogs' % env['root_dir']
    os.makedirs(output_path, exist_ok=True)
    shutil.copyfile(logFile, f'{output_path}/{make_log_filename(test_suite_name)}')

    with open(logFile,"r") as oldFile:
        isAutotestRun = 0
        for line in oldFile:
            if 'Automation Test Succeeded ' in line:
                print("[ OK ] " + (line.rsplit('Display: ',1)[1]))
                isAutotestRun=1
            elif 'Automation Test Failed ' in line:
                print("[ FAILED ] " + (line.rsplit('Display: ',1)[1]))
                isAutotestRun=1

        if not isAutotestRun :
            print("[ WARNING ] " + ("Tests "+ test_suite_name +" doesn't run"))


def launch_test_suite(test_suite_name, env, timeout, enable_render):
    game_path = get_game_path(env)

    commands = f'-ExecCmds="Automation RunTests {test_suite_name}"'
    test_exit = '-TestExit="Automation Test Queue Empty"'
    logs = '-log'
    logsName = f'-log={make_log_filename(test_suite_name)}'

    args = [game_path, '-noP4', '-buildmachine', '-unattended', commands, test_exit, logs, logsName]
    args.extend(['-windowed', '-resx=1024', '-resy=768'])

    if not enable_render:
        args.append('-nullrhi')

    print (f'Running test {test_suite_name}...')

#   if builded game does't exist use this args:
#   editorPath=env['editor_path']
#   projectPath=env['project_path']
#   game_mode = '-Game'
#   args=[editorPath,projectPath,game_mode,commands,testExit,logs,logsName ]

    retcode = subprocess.call(' '.join(args), timeout=timeout*60, shell=True)
    write_logs(test_suite_name,env)

    return retcode


def run_tests(tests, enable_render):
    tests_passed = True
    for test in tests:
        retcode = launch_test_suite(test, env, test_config['Timeout'], enable_render)
        if retcode != 0:
            print (f'Test {test} failed!')
            tests_passed = False

    return tests_passed


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='AutoTests')
    AutomationCommon.add_target_argument_to_parser(parser)
    parser.add_argument('--include_graphical_tests', action='store_true')
    parser.add_argument('--graphical_tests_only', action='store_true')
    parser.add_argument('--force_enable_render', action='store_true')

    args = parser.parse_args()
    env = AutomationCommon.get_environment(args.target)
    test_config = AutomationCommon.get_config()['AutoTests']
    tests_passed = True

    if not args.graphical_tests_only:
        should_enable_render = args.force_enable_render
        tests_passed = run_tests(test_config['Tests'], enable_render=should_enable_render)

    if args.graphical_tests_only or args.include_graphical_tests:
        tests_passed = run_tests(test_config['GraphicalTests'], enable_render=True) and tests_passed

    sys.exit(0 if tests_passed else 1)
