import os
import shutil
from Hephaestus.Addons.GitFetcher import GitFetcher
from Hephaestus.BaseBuilder import BaseBuilder
from Hephaestus.Configuration import Platform
from Hephaestus.Utils import run_script


name = 'Unreal Engine'
version = '4.25.1'
type = 'release'
dependencies = ['android_ndk', 'android_sdk', 'jre']

fetcher = GitFetcher('http://gitlab.igro-tek.com/igrotek/unreal-engine.git', branch=f'tags/{version}-{type}')

class UEBuilder(BaseBuilder):
    def build(self, content_path, build_path):
        # initial setup - download and install dependencies
        run_script('Setup')

        # make an installed build
        args = ['BuildGraph',
                '-target=Make Installed Build %s' % self.get_host_platform_name(),
                '-script=./Engine/Build/InstalledEngineBuild.xml',
                '-set:CompileDatasmithPlugins=true',
                '-set:WithFullDebugInfo=true',
                '-set:WithClient=true',
                '-set:WithServer=true',
                '-set:VS2019=true',
                '-set:WithDDC=false',
                '-clean']

        args.extend(self.get_additional_platform_config())

        run_script('Engine/Build/BatchFiles/RunUAT', args)
        self.finalize(content_path, build_path)

    def get_additional_platform_config(self):
        # disable linux cross-compiling on windows, hololens and lumin
        if Config.platform == Platform.Windows:
            return ['-set:WithLinux=false', 
                    '-set:WithLinuxAArch64=false', 
                    '-set:WithLumin=false', 
                    '-set:WithWin32=false', 
                    '-set:WithHoloLens=false']
        return []

    def get_host_platform_name(self):
        if Config.platform == Platform.Windows:
            return 'Win64'
        elif Config.platform == Platform.Mac:
            return 'Mac'
        elif Config.platform == Platform.Linux:
            return 'Linux'

    def get_os_name(self):
        if Config.platform == Platform.Windows:
            return 'Windows'
        elif Config.platform == Platform.Mac:
            return 'Mac'
        elif Config.platform == Platform.Linux:
            return 'Linux'

    def finalize(self, content_path, build_path):
        # move to build path
        installed_build_path = f'{content_path}/LocalBuilds/Engine/{self.get_os_name()}'

        for item in os.listdir(installed_build_path):
            shutil.move(f'{installed_build_path}/{item}', build_path)
        
        # remove sources
        shutil.rmtree(content_path, ignore_errors=True)

builder = UEBuilder()
