import os
import shutil
from Hephaestus.Addons.PackageFetcher import PackageFetcher
from Hephaestus.BaseBuilder import BaseBuilder
from Hephaestus.Configuration import Platform
from Hephaestus.Utils import run_script



name = 'Android SDK'
version = '6200805'
dependencies = ['jre', 'android_ndk']

api_level = '28'
build_tools_velsion = '%s.0.3' % api_level 
android_sdk_license = '24333f8a63b6825ea9c5514f83c2829b004d1fee'

url = {
    Platform.Windows: f'https://dl.google.com/android/repository/commandlinetools-win-{version}_latest.zip',
    Platform.Mac: f'https://dl.google.com/android/repository/commandlinetools-mac-{version}_latest.zip',
    Platform.Linux: f'https://dl.google.com/android/repository/commandlinetools-linux-{version}_latest.zip'
}

fetcher = PackageFetcher(url)

# according to https://stackoverflow.com/a/61176718
class AndroidSDKInstaller(BaseBuilder):
    def build(self, content_path, build_path):

        # move tools folder inside cmdline-tools folder
        cmdline_tools = f'{content_path}/cmdline-tools'
        os.mkdir(cmdline_tools)
        shutil.move('tools', cmdline_tools)

        # accept licenses
        os.mkdir(f'{content_path}/licenses')
        with open(f'{content_path}/licenses/android-sdk-license', 'w') as text_file:
            text_file.write(android_sdk_license)

        # setup environment
        os.environ['ANDROID_HOME'] = content_path

        # install tools
        sdkmanager = f'{cmdline_tools}/tools/bin/sdkmanager'
        args = ['platform-tools',
                'platforms;android-%s' % api_level,
                'build-tools;%s' % build_tools_velsion,
                'extras;google;google_play_services',
                'extras;google;market_apk_expansion',
                'extras;google;market_licensing',
                'extras;google;usb_driver',
                'ndk-bundle']
        run_script(sdkmanager, args)
        self.install_android_ndk_as_bundle(content_path)

    def install_android_ndk_as_bundle(self, content_path):
        print ('Replacing installed ndk-bundle on dependant ndk...')

        # remove old ndk-bundle
        ndk_bundle_path = f'{content_path}/ndk-bundle'
        shutil.rmtree(ndk_bundle_path)

        # copy given ndk to ndk-bunlde path
        ndk_path = self.own_module.resolved_dependencies['android_ndk'].module_content_dir
        shutil.copytree(ndk_path, ndk_bundle_path)
            

builder = AndroidSDKInstaller()

def get_env_variables(content_path, build_path, env_writer):
    env_writer.set('ANDROID_HOME', content_path)
