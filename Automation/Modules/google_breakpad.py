import os
import shutil
from Hephaestus.Addons.GitFetcher import GitFetcher
from Hephaestus.BaseBuilder import BaseBuilder
from Hephaestus.Utils import run_script


name = 'Google Breakpad'
version = '64'
dependencies = ['android_ndk']

fetcher = GitFetcher('https://github.com/google/breakpad.git', branch=f'chrome_{version}')

class AndroidNDKBuilder(BaseBuilder):
    def build(self, content_path, build_path):
        os.chdir(f'{content_path}/android/sample_app/jni')
        run_script('ndk-build.cmd')

builder = AndroidNDKBuilder()
