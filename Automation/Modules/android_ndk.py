from Hephaestus.Addons.PackageFetcher import PackageFetcher
from Hephaestus.Addons.PackagePathFixer import PackagePathFixer
from Hephaestus.Configuration import Platform


name = 'Android NDK'
version = 'r21b'

url = {
    Platform.Windows: f'https://dl.google.com/android/repository/android-ndk-{version}-windows-x86_64.zip',
    Platform.Mac: f'https://dl.google.com/android/repository/android-ndk-{version}-darwin-x86_64.zip',
    Platform.Linux: f'https://dl.google.com/android/repository/android-ndk-{version}-linux-x86_64.zip'
}

fetcher = PackageFetcher(url)
builder = PackagePathFixer()

def get_env_variables(content_path, build_path, env_writer):
    env_writer.set('NDKROOT', content_path)
    env_writer.set('NDK_ROOT', content_path)
