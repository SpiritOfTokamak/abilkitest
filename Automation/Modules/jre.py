from Hephaestus.Addons.PackageFetcher import PackageFetcher
from Hephaestus.Addons.PackagePathFixer import PackagePathFixer
from Hephaestus.Configuration import Platform


name = 'OpenJDK'
version = '8.252'

url = {
    Platform.Windows: 'https://github.com/AdoptOpenJDK/openjdk8-binaries/releases/download/jdk8u252-b09.1/OpenJDK8U-jdk_x64_windows_hotspot_8u252b09.zip',
    Platform.Mac: 'https://github.com/AdoptOpenJDK/openjdk8-binaries/releases/download/jdk8u252-b09.1/OpenJDK8U-jdk_x64_mac_hotspot_8u252b09.tar.gz',
    Platform.Linux: 'https://github.com/AdoptOpenJDK/openjdk8-binaries/releases/download/jdk8u252-b09/OpenJDK8U-jdk_x64_linux_hotspot_8u252b09.tar.gz'
}

fetcher = PackageFetcher(url)
builder = PackagePathFixer()

def get_env_variables(content_path, build_path, env_writer):
    env_writer.set('JAVA_HOME', content_path)
    env_writer.append('PATH', f'{content_path}/bin')
