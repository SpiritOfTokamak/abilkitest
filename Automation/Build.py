import argparse
import os
import shutil
import subprocess
import sys
import zipfile
import AutomationCommon


def make_build_info(env):
    content = 'Branch: %s\n' % env['branch']
    content += 'Commit: %s\n' % env['commit']
    content += 'Message: %s\n' % env['commit_message']

    return content


def write_build_info(env):
    print('----------- Writing build info -----------')
    build_info = make_build_info(env)
    build_info_path = os.path.join(env['platform_release_path'], 'build_info.txt')
	
    build_info_file = open(build_info_path, 'w', encoding='utf8')
    build_info_file.write(build_info)
    build_info_file.close()


def run_ubt(env, ubt_args):
    ubt_path = "%s/Engine/Binaries/DotNET/UnrealBuildTool.exe" % env['ue_path']
    args = []

    if env['host_platform'] != 'Win64':
        args = ["%s/Engine/Build/BatchFiles/%s/RunMono.sh" % (env['ue_path'], env['host_platform'])]

    args.append(ubt_path)
    args.extend(ubt_args)
    subprocess.check_call(args)


def build_editor(env):
    print('----------- Building editor target -----------')
    args = ['Development', env['host_platform'], '-Project=%s' % env['project_path'], '-TargetType=Editor', '-NoHotReloadFromIDE']
    run_ubt(env, args)


def build_project(env):
    build_target = env['build_target']
    print(f'----------- Building project target {build_target.name} -----------')

    args = [env['runuat_path'],
            'BuildCookRun',
            '-project=\"%s\"' % env['project_path'],
            '-noP4',
            '-targetplatform=%s' % build_target.platform,
            '-clientconfig=%s' % build_target.configuration,
            '-serverconfig=%s' % build_target.configuration,
            '-cook',
            '-build',
            '-stage',
            '-pak',
            '-compressed',
            '-prereqs',
            '-package',
            '-archive',
            '-archivedirectory=\"%s\"' % env['release_path'],
            '-SkipCookingEditorContent',
            '-installed',
            '-target=%s' % env['project_name'],
            '-NoCompileEditor']
    
    args.extend(build_target.uat_additional_arguments)
    subprocess.check_call(args)


def cleanup(env):
    root = env['root_dir']
    remove_dir = f'{root}/Intermediate/Android'
    
    if os.path.exists(remove_dir):
        print (f'Remove dir: {remove_dir}')
        shutil.rmtree(remove_dir, ignore_errors=True)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Build project')
    AutomationCommon.add_target_argument_to_parser(parser)
    parser.add_argument('--setup_environment', action='store_true', help='Setup build environment: build UE, download SDK, NDK. See modules directory')
    parser.add_argument('--cleanup', action='store_true', help='Clean some directories after build')

    args = parser.parse_args()
    env = AutomationCommon.get_environment(args.target, args.setup_environment)

    build_editor(env)
    build_project(env)

    if args.cleanup:
        cleanup(env)

    if env['CI']:
        write_build_info(env)
