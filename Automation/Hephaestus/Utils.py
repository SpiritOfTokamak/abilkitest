import errno
import os
import shutil
import stat
import subprocess
from Hephaestus.Configuration import Platform


def remove_dir(path):
    if os.path.isdir(path):
        shutil.rmtree(path, onerror=remove_readonly)


def remove_readonly(func, path, exc):
    excvalue = exc[1]
    if func in (os.rmdir, os.remove, os.unlink) and excvalue.errno == errno.EACCES:
        # change the file to be readable,writable,executable: 0777
        os.chmod(path, stat.S_IRWXU | stat.S_IRWXG | stat.S_IRWXO)
        # retry
        func(path)
    else:
        raise RuntimeError(excvalue)


def get_files(directory, extension=None):
    file_paths = []

    for root, directories, files in os.walk(directory):
        for filename in files:
            if extension and not filename.endswith(extension):
                continue

            filepath = os.path.join(root, filename)
            file_paths.append(filepath.replace('\\', '/'))

    return file_paths


# todo: DRY!!!
def find_executable_file(directory, name):
    for root, directories, files in os.walk(directory):
        for filename in files:
            if filename == name and os.access(os.path.join(root, filename), os.X_OK):
                return os.path.join(root, filename)

    return None


def find_file(directory, name):
    for root, directories, files in os.walk(directory):
        for filename in files:
            if filename == name:
                return os.path.join(root, filename)

    return None


def file_name(path):
    basename = os.path.basename(path)
    return ''.join(basename.split('.')[:-1])


def get_module_attr(module, attribute):
    return getattr(module, attribute) if hasattr(module, attribute) else None


def run_subprocess(path, args, extension, use_shell):
    call_path = path if path.endswith(extension) else '%s%s' % (path, extension)
    if not call_path.startswith('./') and not os.path.isabs(call_path):
        call_path = './%s' % call_path

    if Config.platform == Platform.Windows:
        call_path = call_path.replace('/', '\\')

    call_args = [call_path]
    call_args.extend(args)

    subprocess.check_call(call_args, shell=use_shell)


def run_script(script, args = []):
    run_subprocess(script, args, Config.shell_script_extension, True)


def run_executable(executable, args = []):
    run_subprocess(executable, args, Config.executable_extension, False)
