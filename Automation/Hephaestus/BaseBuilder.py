class BaseBuilder:
    own_module = None
    dependencies = []

    def build(self, sources_path, build_path):
        pass