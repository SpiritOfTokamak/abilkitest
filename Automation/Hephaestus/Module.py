import filecmp
import shutil
import os
from os.path import isfile, join, normpath
from Hephaestus.Utils import get_files, file_name, remove_dir, get_module_attr


class Module:
    module_script_file = ''
    module_dir = ''
    module_name = ''
    module = None
    module_content_dir = ''
    module_build_dir = ''
    success_tag = ''
    artifacts = None
    dependencies = []
    # list of Module class objects
    resolved_dependencies = {}
    env_variables_getter = None

    # TODO: module_name from module_script_file
    def __init__(self, module_name, module_script_file, module_dir):
        self.module_script_file = module_script_file
        self.module_name = module_name
        self.module = __import__(module_name)
        self.module_dir = join(module_dir, self.get_version())
        self.module_content_dir = join(self.module_dir, 'content')
        self.module_build_dir = join(self.module_dir, 'build')
        self.artifacts = get_module_attr(self.module, 'artifacts')
        self.env_variables_getter = get_module_attr(self.module, 'get_env_variables')

        if self.artifacts:
            self.artifacts.module_info = self

        module_dependencies = get_module_attr(self.module, 'dependencies')
        self.dependencies = module_dependencies if module_dependencies is not None else []

        if self.can_be_built():
            self.success_tag = join(self.module_build_dir, 'success_build_tag')
            builder = get_module_attr(self.module, 'builder')
            builder.own_module = self
            self.dependencies.extend(builder.dependencies)
        else:
            self.success_tag = join(self.module_dir, 'success_build_tag')

    def get_module_name(self):
        return self.module_name

    def get_public_name(self):
        name_attr = get_module_attr(self.module, 'name')
        return name_attr if name_attr is not None else self.get_module_name()

    def get_version(self):
        return get_module_attr(self.module, 'version')

    def is_built(self):
        return isfile(self.success_tag) and filecmp.cmp(self.module_script_file, self.success_tag)

    def fetch(self):
        remove_dir(self.module_content_dir)
        fetcher = get_module_attr(self.module, 'fetcher')
        fetcher.fetch(self.module_content_dir)

        if not self.can_be_built():
            shutil.copyfile(self.module_script_file, self.success_tag)

    def can_be_built(self):
        return get_module_attr(self.module, 'builder') is not None

    def build(self):
        remove_dir(self.module_build_dir)
        os.mkdir(self.module_build_dir)
        os.chdir(self.module_content_dir)

        builder = get_module_attr(self.module, 'builder')
        builder.build(self.module_content_dir, self.module_build_dir)

        build_verifier = get_module_attr(self.module, 'build_verifier')
        if build_verifier:
            build_verifier.verify(self.module_build_dir)

        shutil.copyfile(self.module_script_file, self.success_tag)

    def get_env(self, env_writer):
        if self.env_variables_getter != None:
            self.env_variables_getter(self.module_content_dir, self.module_build_dir, env_writer)
