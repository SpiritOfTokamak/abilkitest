import builtins
import os
import sys
from os.path import isfile, join
from Hephaestus.EnvironmentVariableWriter import EnvironmentVariableWriter
from Hephaestus.Configuration import Configuration
from Hephaestus.Module import Module


class Hephaestus:
    modules_path = ''
    build_path = ''
    modules = {}

    def __init__(self, modules_path, build_path):
        self.modules_path = modules_path
        self.build_path = build_path
        builtins.Config = Configuration(modules_path, build_path)

        self.find_modules()

    def find_modules(self):
        module_files = [f for f in os.listdir(self.modules_path) if isfile(join(self.modules_path, f)) and f.endswith('.py')]
        sys.path.append(self.modules_path)

        # Process modules
        for module_file in module_files:
            module_name = ''.join(module_file.split('.')[:-1])
            module_dir = join(self.build_path, module_name)
            module_script = join(self.modules_path, module_file)
            module = Module(module_name, module_script, module_dir)

            self.modules[module_name] = module

        # resolve dependencies
        for _, module in self.modules.items():
            for dep in module.dependencies:
                module.resolved_dependencies[dep] = self.modules[dep]

    def build_module(self, module_to_build):
        self.build_modules([module_to_build])

    # todo: reimplement dependency resolving mechanism
    def build_modules(self, modules_to_build):
        env_writer = EnvironmentVariableWriter()
        build_task = self.make_build_task(modules_to_build)
        build_task_str = ' '.join(build_task)

        build_task = list(map(lambda t: self.modules[t], build_task))
        self.write_modules_env(build_task, env_writer)

        build_task = [task for task in build_task if not task.is_built()]
        if len(build_task) == 0:
            print ('Nothing to do')
            return

        print(f'Modules to build: {build_task_str}')

        i = 1
        for task in build_task:
            print (f'[{i}/{len(build_task)}] Build module {task.module_name}')
            i += 1

            task.fetch()

            if task.can_be_built():
                task.build()

            self.write_modules_env([task], env_writer)

    def write_modules_env(self, modules, writer):
        for module in modules:
            if module.is_built():
                module.get_env(writer)

    def make_build_task(self, modules_to_build):
        build_task = []

        for module_to_build in modules_to_build:
            module_build_task = [module_to_build]
            dependencies = self.get_module_dependencies_raw(module_to_build)
            module_build_task.extend(dependencies)

            module_build_task.reverse()
            build_task.extend(module_build_task)
            
        build_task_final = []
        [build_task_final.append(x) for x in build_task if x not in build_task_final]
        return build_task_final

    def get_module_dependencies_raw(self, module_name):
        result = []
        module = self.modules[module_name]

        for module_dependency in module.dependencies:
            dependencies = self.get_module_dependencies_raw(module_dependency)
            result.append(module_dependency)
            result.extend(dependencies)

        return result

    def get_module_version(self, module_name):
        return self.modules[module_name].get_version()

    def get_module_content_path(self, module_name):
        return self.modules[module_name].module_content_dir

    def get_module_build_path(self, module_name):
        return self.modules[module_name].module_build_dir
