from enum import Enum
from Hephaestus.Utils import find_file


class ArtifactType(Enum):
    Executable = 0
    Include = 1
    SourceFile = 2
    Data = 3

class ArtifactExplorer:
    artifacts = None
    module_info = None

    def __init__(self, artifacts):
        self.artifacts = artifacts

    def explore(self, artifact_type, name):
        paths = [self.module_info.module_content_dir]
        if self.module_info.can_be_built():
            paths.append(self.module_info.module_build_dir)

        artifact = self.artifacts[artifact_type][name]

        # todo: bld
        if artifact_type == ArtifactType.Executable:
            for path in paths:
                file = find_file(path, artifact + self.module_info.configuration.executable_extension)
                if file:
                    return file
        return None
