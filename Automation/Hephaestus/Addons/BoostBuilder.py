import os
from Hephaestus.BaseBuilder import BaseBuilder
from Hephaestus.Utils import run_executable, run_script


class BoostBuilder(BaseBuilder):
    def build(self, content_path, build_path):
        if not os.path.isdir(build_path):
            os.mkdir(build_path)
        os.chdir(content_path)

        run_script('bootstrap')
        run_executable('b2', ['headers', '-j%u' % Config.processors_count])
        address_model = 64 if Config.is_x64_address_model else 32

        build_args = ['toolset=%s-%s' % (Config.toolset, Config.toolset_version),
                      'address-model=%d' % address_model,
                      'link=static',
                      'threading=multi',
                      'runtime-link=static',
                      '--build-dir=%s' % build_path,
                      '--stagedir=%s' % build_path,
                      '-j%u' % Config.processors_count]
        run_executable('b2', build_args)