import os
import shutil
from Hephaestus.BaseBuilder import BaseBuilder


class PackagePathFixer(BaseBuilder):
    def build(self, content_path, build_path):
        subpath = os.listdir(content_path)[0]
        path = f'{content_path}/{subpath}'

        for item in os.listdir(path):
            shutil.move(f'{path}/{item}', content_path)
        os.rmdir(path)