import os
import shutil
import sys
import tarfile
import zipfile
from Hephaestus.Addons.Downloader import Downloader
from Hephaestus.Utils import remove_dir


class PackageFetcher:
    url = None     

    def __init__(self, url):
        self.url = url
    
    def open_archive(self, filename):
        if zipfile.is_zipfile(filename):
            return zipfile.ZipFile(filename, 'r')
        elif tarfile.is_tarfile(filename):
            return tarfile.open(filename)
        else:
            return None

    def fetch(self, path):
        remove_dir(path)
        os.makedirs(path)

        downloader = Downloader()
        if type(self.url) is dict:
            url = self.url[Config.platform]
        else:
            url = self.url

        filename = downloader.download(url, path)

        as_archive = self.open_archive(filename)
        if as_archive:
            print ('Unpacking %s' % filename)
            with as_archive as archive:
                archive.extractall(path)
            os.remove(filename)
