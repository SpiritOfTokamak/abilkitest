import os
import sys
import urllib.request


# Print iterations progress
def print_progress_bar(iteration, total, prefix = '[', suffix = ']', decimals = 1, length = 100, fill = '#'):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    sys.stdout.write('%s |%s| %s%% %s\r' % (prefix, bar, percent, suffix))
    sys.stdout.flush()
    # Print New Line on Complete
    if iteration == total:
        print('\n')


class Downloader:
    def download(self, url, path):
        filename = os.path.basename(url)
        filepath = os.path.join(path, filename)

        with open(filepath, 'wb') as file:
            opened_url = urllib.request.urlopen(url)
            meta = opened_url.info()
            file_size = int(meta["Content-Length"])
            print ("Downloading %s; length %s bytes" % (filename, file_size))

            file_size_dl = 0
            block_sz = 512 * 1024
            while True:
                buffer = opened_url.read(block_sz)
                if not buffer:
                    break

                file_size_dl += len(buffer)
                print_progress_bar(file_size_dl, file_size)
                file.write(buffer)

        return filepath
