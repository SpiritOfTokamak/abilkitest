import os
import subprocess
from Hephaestus.Utils import remove_dir


# TODO: add checkout of specified revision
class GitFetcher:
    repo_url = ""
    branch = None
    is_tag = False

    def __init__(self, repo_url, branch=None):
        self.repo_url = repo_url
        self.branch = branch
        self.is_tag = branch.startswith('tags/') if branch else False

    def fetch(self, path):
        remove_dir(path)

        # Clone the repo
        clone_cmd = ['git', 'clone', '--depth=1', self.repo_url, path]
        if subprocess.call(clone_cmd) != 0:
            raise Exception('Cannot clone %s' % self.repo_url)
        os.chdir(path)

        # Checkouting
        checkout_cmd = ['git', 'checkout', self.branch]
        fetch_tag_cmd = None
        if self.branch:
            fetch_tag_cmd = ['git', 'fetch', '--depth=1', 'origin', '%s:%s' % (self.branch, self.branch)]

        if self.branch:
            # Try to checkout branch
            if subprocess.call(fetch_tag_cmd) != 0 or subprocess.call(checkout_cmd) != 0:
                raise Exception('Cannot checkout tag %s' % self.branch)

        # Try to init submodules
        submodules_cmd = ['git', 'submodule', 'update', '--recursive', '--init', '--depth=1', '--jobs=8']
        # TODO: remove this shit
        if 'boost' not in self.repo_url:
            submodules_cmd = ['git', 'submodule', 'update', '--recursive', '--init', '--jobs=8']

        if subprocess.call(submodules_cmd) != 0:
            result = False
            # Ok, it's fail. Try to fetch like a branch
            if self.branch:
                submodules_foreach_cmd = ['git', 'submodule', 'foreach', '--recursive']
                submodules_checkout_cmd = '\"\"%s; %s\"\"' % (' '.join(fetch_tag_cmd), ' '.join(checkout_cmd))
                submodules_foreach_cmd.append(submodules_checkout_cmd)
                result = subprocess.call(submodules_foreach_cmd) == 0

            # after all perform full update
            if not result:
                full_update_cmd = ['git', 'submodule', 'update', '--recursive', '--init', '--force', '--jobs=8']
                result = subprocess.call(full_update_cmd) == 0

            if not result:
                raise Exception('Cannot init submodules')

        # clean up
        remove_dir(os.path.join(path, '.git'))
