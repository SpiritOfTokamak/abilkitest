import os
import subprocess
from Hephaestus.ArtifactExplorer import ArtifactType
from Hephaestus.BaseBuilder import BaseBuilder
from Hephaestus.Configuration import Platform
from Hephaestus.Addons.PackageFetcher import PackageFetcher
from Hephaestus.Utils import remove_dir


class CmakeBuilder(BaseBuilder):
    cmake_definitions = []
    
    def __init__(self, cmake_definitions=[]):
        self.cmake_definitions = cmake_definitions
        self.dependencies = ['cmake']

    def create_path(self, path):
        if not os.path.isdir(path):
            remove_dir(path)
            os.makedirs(path)

    def get_debug_path(self, path):
        return os.path.join(path, 'Debug')

    def get_release_path(self, path):
        return os.path.join(path, 'Release')

    def generate_project(self, args, path):
        self.create_path(path)
        os.chdir(path)
        if subprocess.call(args) != 0:
            raise Exception('Cannot generate CMake project')

    def generate_debug_project(self, args, path):
        debug_args = list(args)
        debug_args.append('-DCMAKE_BUILD_TYPE=Debug')
        self.generate_project(debug_args, self.get_debug_path(path))

    def generate_release_project(self, args, path):
        release_args = list(args)
        release_args.append('-DCMAKE_BUILD_TYPE=Release')
        self.generate_project(release_args, self.get_release_path(path))

    def build_project(self, args, path, config=''):
        build_args = list(args)
        debug_build_path = self.get_debug_path(path) 
        release_build_path = self.get_release_path(path)

        if config == 'Debug':
            build_args.extend([debug_build_path, '--config', 'Debug'])
        elif config == 'Release':
            build_args.extend([release_build_path, '--config', 'Release'])
        else:
            self.build_project(args, path, 'Debug')
            self.build_project(args, path, 'Release')
            return

        if subprocess.call(build_args):
            raise Exception('Cannot build CMake project')

    def build(self, sources_path, build_path):
        cmake_artifacts = self.own_module.resolved_dependencies['cmake'].artifacts
        cmake_executable = cmake_artifacts.explore(ArtifactType.Executable, 'cmake')

        cmake_arguments = [cmake_executable, sources_path]
        for cmake_definition in self.cmake_definitions:
            cmake_arguments.append('-D' + cmake_definition)
        cmake_build_arguments = [cmake_executable, '--build']

        # generate project
        self.generate_debug_project(cmake_arguments, build_path)
        self.generate_release_project(cmake_arguments, build_path)

        # build under all configuration
        self.build_project(cmake_build_arguments, build_path)
