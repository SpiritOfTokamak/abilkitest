import multiprocessing
import sys
from enum import Enum


class Platform(Enum):
    Windows = 0
    Linux = 1
    Mac = 2


class Configuration:
    executable_extension = ''
    static_lib_extension = ''
    shared_lib_extension = ''
    modules_path = ''
    output_path = ''
    platform = Platform
    processors_count = multiprocessing.cpu_count()
    is_x64_address_model = True
    shell_script_extension = ''

    def __init__(self, modules_path, output_path):
        if sys.platform.startswith('win32'):
            self.platform = Platform.Windows
        elif sys.platform.startswith('linux'):
            self.platform = Platform.Linux
        elif sys.platform.startswith('darwin'):
            self.platform = Platform.Mac
        else:
            raise Exception('Platform %s is unsupported' % sys.platform)

        self.executable_extension = '.exe' if self.platform == Platform.Windows else ''
        self.static_lib_extension = '.lib' if self.platform == Platform.Windows else '.a'
        self.shared_lib_extension = '.dll' if self.platform == Platform.Windows else '.so'
        self.shell_script_extension = '.bat' if self.platform == Platform.Windows else '.sh'
        self.modules_path = modules_path
        self.output_path = output_path
