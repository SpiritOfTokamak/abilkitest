import os


class EnvironmentVariableWriter:
    def set(self, name, val):
        os.environ[name] = val

    def append(self, name, val):
        os.environ[name] = '%s;%s' % (val, os.environ[name])

class EnvironmentVariableSaver:
    set_vars = {}
    appended_vars = {}

    def set(self, name, val):
        self.set_vars[name] = val

    def append(self, name, val):
        if name in self.appended_vars:
            self.appended_vars[name].append(val)
        else:
            self.appended_vars[name] = [val]
