import appcenter
import argparse
import glob
import os
import zipfile
import AutomationCommon


def get_build_num(platform, env, appcenter_config):
    if platform == 'android':
        engine_config = AutomationCommon.load_config('DefaultEngine.ini')

        arch = 'ArmV7' if appcenter_config['PreferredAndroidArch'] == 'armv7' else 'Arm64'
        version = AutomationCommon.to_int(AutomationCommon.get_config_value(engine_config, f'StoreVersion'))
        offset = AutomationCommon.to_int(AutomationCommon.get_config_value(engine_config, f'StoreVersionOffset{arch}'))

        print(f'Version {str(version + offset)}')
        return str(version + offset)
    else:
        return str(AutomationCommon.to_int(env['build_num']))
        

def get_version_str_from_config(env, config):
    return AutomationCommon.get_version_str(config['MajorVersion'], config['MinorVersion'])


def publish_android(client, env, appcenter_config):
    apks = glob.glob(os.path.join(env['platform_release_path'], '*.apk'))
    target_apk = None
    for apk in apks:
        if appcenter_config['PreferredAndroidArch'] in apk:
            target_apk = apk
            break

    if target_apk is None:
        target_apk = apks[0]
    print(f'Uploading {target_apk} to AppCenter...')

    release_notes = prepare_release_notes(client, appcenter_config)

    client.versions.upload_and_release(
        owner_name=appcenter_config['Owner'],
        app_name=appcenter_config['AppName'],
        version=get_version_str_from_config(env, config),
        build_number=get_build_num('android', env, appcenter_config),
        binary_path=target_apk,
        group_id=appcenter_config['DistributeGroup'],
        release_notes=release_notes,
        branch_name=env['branch'],
        commit_hash=env['commit'],
        commit_message=env['commit_message']
    )


def publish(client, target, env, appcenter_config):
    if target.platform == 'Android':
        publish_android(client, env, appcenter_config)
    else:
        raise Exception(f'Unsupported platform for publishing: {target.platform}')


def get_symbol_file_extension(target):
    if target.platform == 'Android':
        return ".so"
    elif target.platform == 'Win64':
        return ".pdb"
    else:
        raise Exception(f'Unsupported platform for symbol gathering: {target.platform}')


def prepare_symbols(build_target, env):
    binaries_path = os.path.join(env['root_dir'], 'Binaries', build_target.platform)
    symbol_extension = get_symbol_file_extension(build_target)
    symbols = glob.glob(os.path.join(binaries_path, f'*{symbol_extension}'))
    symbols_archive = os.path.join(env['root_dir'], 'symbols.zip')
    print(f'Packaging symbols to {symbols_archive}...')

    with zipfile.ZipFile(symbols_archive, 'w', zipfile.ZIP_DEFLATED, compresslevel=9) as archive:
        for symbol in symbols:
            so_name = os.path.basename(symbol)
            abi = 'armv7' if 'armv7' in symbol else 'arm64'
            arcsymbol = os.path.join('local', abi, so_name)

            print(f'Adding {so_name} as {arcsymbol}')
            archive.write(symbol, arcsymbol)
    
    return symbols_archive


def upload_symbols(client, build_target, env, appcenter_config):
    symbols = prepare_symbols(build_target, env)

    print('Uploading symbols to AppCenter...')
    client.crashes.upload_symbols(
        owner_name=appcenter_config['Owner'],
        app_name=appcenter_config['AppName'],
        symbols_path=symbols,
        symbol_type=appcenter.models.SymbolType.breakpad,
        version=get_version_str_from_config(env, config),
        build_number=get_build_num(build_target.platform.lower(), env, appcenter_config))


def prepare_release_notes(client, appcenter_config):
    latest_commit = client.versions.latest_commit(
        owner_name=appcenter_config['Owner'],
        app_name=appcenter_config['AppName'])
    history = AutomationCommon.execute_git(['log', '--oneline', '--no-decorate', f'{latest_commit}..'])

    notes = ''
    for commit in history:
        commit_str = str(commit)
        if 'Merge branch' not in commit_str:
            commit_str = ' '.join(commit_str.split(' ')[1:])
            if commit_str.endswith('\\n\''):
                commit_str = commit_str[0:-3]
            notes = notes + commit_str + '\n'

    # print (notes)
    # return notes
    return "New release"


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='AppCenter actions script')
    AutomationCommon.add_target_argument_to_parser(parser)
    parser.add_argument('--publish', action='store_true')
    parser.add_argument('--upload_symbols', action='store_true')

    args = parser.parse_args()
    env = AutomationCommon.get_environment(args.target)
    config = AutomationCommon.get_config()
    appcenter_config = config['AppCenter']
    client = appcenter.AppCenterClient(access_token=appcenter_config['APIToken'])
    build_target = env['build_target']

    if args.publish:
        publish(client, build_target, env, appcenter_config)

    if args.upload_symbols:
        upload_symbols(client, build_target, env, appcenter_config)
