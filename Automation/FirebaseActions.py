import argparse
import os
import subprocess
import AutomationCommon


def upload_symbols(env):
    build_target = env['build_target']

    if build_target.platform == 'Android':
        upload_symbols_android(build_target, env)
    else:
        raise Exception(f'Unsupported platform: {build_target.platform}')


def upload_symbols_android(build_target, env):
    intermediate_dir = '%s/Intermediate/Android' % env['root_dir']
    symbol_type = 'Release' if build_target.configuration == 'Shipping' else 'Debug'

    for arch_path in os.listdir(intermediate_dir):
        item_path = f'{intermediate_dir}/{arch_path}'
        if os.path.isdir(item_path):
            gradle_path = f'{item_path}/gradle'
            gradlew_path = f'{gradle_path}/gradlew'

            subprocess.check_call([gradlew_path, '-p', gradle_path, f'crashlyticsUploadSymbols{symbol_type}'], shell=True)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Firebase actions script')
    AutomationCommon.add_target_argument_to_parser(parser)
    parser.add_argument('--upload_symbols', action='store_true')

    args = parser.parse_args()
    env = AutomationCommon.get_environment(args.target)
    
    if args.upload_symbols:
        upload_symbols(env)