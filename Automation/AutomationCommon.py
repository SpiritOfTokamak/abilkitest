import glob
import os
import json
import pathlib 
import string
import subprocess
import sys
import tempfile
from datetime import datetime
from os.path import expanduser
from BuildTarget import BuildTarget


def find_project():
    files = glob.glob("*.uproject")
    if not files:
        files = glob.glob("../*.uproject")

    if not files or len(files) != 1:
        raise Exception('Unable to find project file')

    return os.path.abspath(files[0])


def get_project_root():
    project = find_project()
    return os.path.dirname(project)


def get_project_name():
    project = find_project()
    return os.path.splitext(os.path.basename(project))[0]


def get_project_ue_version():
    with open(find_project(), 'r') as project_file:
        json_doc = json.load(project_file)
        return json_doc['EngineAssociation'] if 'EngineAssociation' in json_doc else None


def find_ue_path():
    project_ue_version = get_project_ue_version()
    ue_path = None

    if sys.platform.startswith('win32'):
        ue_path = find_ue_path_windows(project_ue_version)
    elif sys.platform.startswith('darwin'):
        ue_path = find_ue_path_mac(project_ue_version)

    if ue_path is None:
        raise Exception(f'Unreal Engine {project_ue_version} is not found in the system!')
    return ue_path


def get_ue_short_version(version):
     if version is not None:
        version_list = version.split('.')
        return f'{version_list[0]}.{version_list[1]}'


def find_ue_path_windows(version):
    import winreg
    short_version = get_ue_short_version(version)

    installed_engines = {}
    with winreg.OpenKeyEx(winreg.HKEY_LOCAL_MACHINE, 'SOFTWARE\\EpicGames\\Unreal Engine', 0, winreg.KEY_READ | winreg.KEY_WOW64_64KEY) as key:
        for i in range(0, winreg.QueryInfoKey(key)[0]):
            skey_name = winreg.EnumKey(key, i)
            with winreg.OpenKey(key, skey_name) as skey:
                installed_engines[skey_name] = winreg.QueryValueEx(skey, 'InstalledDirectory')[0]
            
    if short_version is not None:
        return installed_engines[short_version]


def find_ue_path_mac(version):
    home = expanduser("~")
    installed_dat = f'{home}/Library/Application Support/Epic/UnrealEngineLauncher/LauncherInstalled.dat'
    short_version = get_ue_short_version(version)

    installed_engines = {}
    with open(installed_dat, 'r') as installed_file:
        json_doc = json.load(installed_file)
        installations = json_doc['InstallationList']

        for installation in installations:
            installed_engines[installation['AppName'].replace('UE_', '')] = installation['InstallLocation']

    if short_version is not None:
        return installed_engines[short_version]


def get_build_target(target):
    at_config = get_config()
    build_targets_config = at_config['BuildTargets']

    if target.startswith('.'):
        raise Exception(f'Cannot build generic target: {target}')

    if target not in build_targets_config:
        raise Exception(f'Build target {target} not found')

    return BuildTarget(target, build_targets_config)


def get_dependencies_path(project_dir):
    if sys.platform.startswith('win'):
        return f'{project_dir[0]}:/Dependencies'
    return f'{expanduser("~")}/Dependencies'


def setup_build_environment():
    print ('Setting up build environment...')

    # build android environment
    execute_hephaestus(['build', 'android_ndk', 'android_sdk', 'jre'], True)

    env_vars = execute_hephaestus(['get-modules-env', 'android_ndk', 'android_sdk', 'jre'])
    for env_var in env_vars:
        env_var = env_var.replace('\r', '').replace('\n', '')
        if '+=' in env_var:
            var = env_var.split('+=')
            os.environ[var[0]] = '%s;%s' % (var[1], os.environ[var[0]])
        else:
            var = env_var.split('=')
            os.environ[var[0]] = var[1]


def get_environment(target, setup_environment=False):
    env = {'project_name': get_project_name()}
    env['root_dir'] = get_project_root()

    bat_extension = '.sh'
    exe_extension = ''

    # load build target
    build_target = get_build_target(target)
    env['build_target'] = build_target

    if sys.platform.startswith('win32'):
        bat_extension = '.bat'
        exe_extension = '.exe'
        env['host_platform'] = 'Win64'

    elif sys.platform.startswith('darwin'):
        env['host_platform'] = 'Mac'

    env['exe_extension'] = exe_extension

    if setup_environment:
        setup_build_environment()

    env['ue_path'] = find_ue_path()

    runuat_path = os.path.join(env['ue_path'], 'Engine', 'Build', 'BatchFiles', 'RunUAT%s' % bat_extension)
    env['runuat_path'] = runuat_path

    project_path = os.path.join(env['root_dir'], '%s.uproject' % env['project_name'])
    env['project_path'] = project_path

    editor_path = os.path.join(env['ue_path'], 'Engine', 'Binaries', env['host_platform'], 'UE4Editor%s' % exe_extension)
    env['editor_path'] = editor_path

    release_path = os.path.join(env['root_dir'], build_target.build_path)
    env['release_path'] = release_path

    platform_release_path = os.path.join(release_path, build_target.artifacts_path)
    env['platform_release_path'] = platform_release_path

    env['config'] = 'Development'

    env['CI'] = os.getenv('CI')     
    env['build_num'] = os.getenv('CI_PIPELINE_ID')
    env['branch'] = os.getenv('CI_COMMIT_REF_NAME')
    env['commit'] = os.getenv('CI_COMMIT_SHA')
    env['commit_message'] = os.getenv('CI_COMMIT_MESSAGE')

    return env


def get_config():
    config_file_path = os.path.join(get_project_root(), 'Automation', 'config.json')
    with open(config_file_path, 'r') as config_file:
        return json.load(config_file)


def add_target_argument_to_parser(parser):
    at_config = get_config()
    build_targets_config = at_config['BuildTargets']

    valid_build_targets = []
    for target_name, _ in build_targets_config.items():
        if not target_name.startswith('.'):
            valid_build_targets.append(target_name)

    parser.add_argument('--target', choices=valid_build_targets, required=True, help='target for building')


def to_int(obj):
    return int(obj) if obj else 0


def get_version_str(major_version, minor_version):
    return '%d.%d' % (to_int(major_version), to_int(minor_version))


def execute_hephaestus(args, no_output=False, single_line=False):
    project_dir = get_project_root()
    modules_path = f'{project_dir}/Automation/Modules'
    dependencies_path = get_dependencies_path(project_dir)

    hephaestus_args = ['python', 
        f'{project_dir}/Automation/hephaestus.py', 
        '--build_path=%s' % dependencies_path,
        '--modules_path=%s' % modules_path]

    hephaestus_args.extend(args)
    result = execute_proc(hephaestus_args, no_output)

    if not no_output and single_line:
        result = result[0].replace('\r', '').replace('\n', '')

    return result


def execute_git(args, no_output=False):
    git_args = ['git']
    git_args.extend(args)

    return execute_proc(git_args, no_output)


def execute_proc(args, no_output=False):
    #print ('\tCall proc: %s' % args)
    if no_output:
        outbuf = None
    else:
        outbuf = tempfile.TemporaryFile()

    process = subprocess.Popen(args, stdout=outbuf, shell=True)
    process.wait()

    if no_output:
        results = []
    else:
        outbuf.seek(0)
        results = outbuf.readlines()
        outbuf.close()

    if process.returncode != 0:
        raise Exception('Git execution failed with code %d: %s') % (process.returncode, results)

    str_results = []
    for result in results:
        str_results.append(result.decode('utf8'))

    return str_results


def load_config(config_name):
    ini_path = os.path.join(get_project_root(), 'Config', config_name)
    with open(ini_path, 'r') as ini:
        return ini.readlines()


def save_config(config_name, config):
    ini_path = os.path.join(get_project_root(), 'Config', config_name)
    with open(ini_path, 'w') as ini:
        ini.writelines(config)


def override_config_value(config, key, value):
    for n, elem in enumerate(config):
        elems = elem.split('=')
        if elems[0].lower() == key.lower():
            config[n] = f'{key}={value}\n'


def get_config_value(config, key):
    for _, elem in enumerate(config):
        elems = elem.split('=')
        if elems[0].lower() == key.lower():
            return elems[1]
