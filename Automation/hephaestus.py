import argparse
import os
import sys
from Hephaestus.EnvironmentVariableWriter import EnvironmentVariableSaver


def make_instance(args):
    from Hephaestus.Hephaestus import Hephaestus
    hephaestus = Hephaestus(modules_path=args.modules_path, build_path=args.build_path)

    return hephaestus


def modules_list(hephaestus, args):
    i = 1
    for _, module in hephaestus.modules.items():
        print(f'[{i}/{len(hephaestus.modules)}] Module {module.module_name} version {module.get_version()}')
        i += 1


def build(hephaestus, args):
    hephaestus.build_modules(args.modules)


def get_version(hephaestus, args):
    for module in args.modules:
        print(hephaestus.get_module_version(module))


def get_content_path(hephaestus, args):
    for module in args.modules:
        print(hephaestus.get_module_content_path(module))


def get_build_path(hephaestus, args):
    for module in args.modules:
        print(hephaestus.get_module_build_path(module))


def get_modules_env(hephaestus, args):
    build_task = hephaestus.make_build_task(args.modules)
    env_writer = EnvironmentVariableSaver()

    build_task = list(map(lambda t: hephaestus.modules[t], build_task))
    hephaestus.write_modules_env(build_task, env_writer)

    for key, value in env_writer.set_vars.items():
        print(f'{key}={value}')

    for key, values in env_writer.appended_vars.items():
        for value in values:
            print(f'{key}+={value}')


def add_modules_parser(subparsers, command, handler):
    parser = subparsers.add_parser(command)
    parser.add_argument('modules', nargs='+')
    parser.set_defaults(func=handler)


def run_hephaestus():
    parser = argparse.ArgumentParser(description='Hephaestus module management system')
    parser.add_argument('--modules_path', default=os.path.join(os.getcwd(), 'Modules'))
    parser.add_argument('--build_path', default=os.path.join(os.getcwd(), 'Build'))
    subparsers = parser.add_subparsers(help='commands')

    parser_modules_list = subparsers.add_parser('modules-list')
    parser_modules_list.set_defaults(func=modules_list)

    add_modules_parser(subparsers, 'build', build)
    add_modules_parser(subparsers, 'get-version', get_version)
    add_modules_parser(subparsers, 'get-content-path', get_content_path)
    add_modules_parser(subparsers, 'get-build-path', get_build_path)
    add_modules_parser(subparsers, 'get-modules-env', get_modules_env)

    args = parser.parse_args()
    hephaestus = make_instance(args)

    args.func(hephaestus, args)

    return 0

if __name__ == '__main__':
    result = 0
    try:
        result = run_hephaestus()
    except ImportError as e:
        print ('Import error. Please check modules or install Hephaestus requirements: %s' % e)

    sys.exit(result)