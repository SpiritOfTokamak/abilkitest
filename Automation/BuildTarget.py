import AutomationCommon


class BuildTarget:
    name = ''
    configuration = ''
    build_path = ''
    platform = ''
    artifacts_path = ''
    uat_additional_arguments = []

    def __init__(self, name, config):
        self.name = name
        self.apply_config('.generic', config)
        self.apply_config(name, config)

    def apply_config(self, name, config):
        target_config = config[name]
        
        # load config parent if present
        if 'Extends' in target_config:
            self.apply_config(target_config['Extends'], config)

        # load parameters
        if 'Configuration' in target_config:
            self.configuration = target_config['Configuration']

        if 'BuildPath' in target_config:
            self.build_path = target_config['BuildPath']

        if 'Platform' in target_config:
            self.platform = target_config['Platform']

        if 'ArtifactsPath' in target_config:
            self.artifacts_path = target_config['ArtifactsPath']

        if 'UATAdditionalArguments' in target_config:
            self.uat_additional_arguments.extend(target_config['UATAdditionalArguments'])

        # write configs override
        if 'ConfigOverride' in target_config:
            config_override = target_config['ConfigOverride']

            for config_file, values in config_override.items():
                conf = AutomationCommon.load_config(config_file)

                for param_name, param_value in values.items():
                    AutomationCommon.override_config_value(conf, param_name, param_value)
                
                AutomationCommon.save_config(config_file, conf)
